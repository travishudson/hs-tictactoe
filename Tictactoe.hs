import Data.List (intercalate, sortOn)
import Data.Map (Map)
import qualified Data.Map as Map

-- TODO: solve the problem of moving onto empty board taking a minute and a half
-- TODO: fix lack of going for instant wins: showBestMove "X X O O  "
--       Place an 'X' in the lower right corner

-- Interesting speed test results (1.1 GHz 12" early 2015 Macbook):
-- findBestMove blankBoard             -- 1:36 minutes
-- findBestMoveFast blankBoard         -- 15 seconds
-- alphaBetaRater blankBoard negInf posInf  -- 15 seconds
-- alphaBeta blankBoard negInf posInf  -- 15 seconds

-- how to play: showBestMoveFast board
-- or: showRemainderFast board

-- |
-- | The blank, starting board is just a String of 9 space Chars
-- |
blankBoard :: String
blankBoard = replicate 9 ' '

-- |
-- | A move that is nothing, no move at all
-- |
nullMove :: (Char, Int)
nullMove = (' ', 0)

-- |
-- | Previously evaluated board states are stored in a transposition table. This is an empty one to start with.
-- |
nullMap :: Map String Double
nullMap = Map.fromList []

-- |
-- | Define infinitely extreme board ratings
-- |
posInf = read "Infinity" :: Double
negInf = negate posInf

-- |
-- | Use alpha beta pruning to produce the best move, given an initial board state
-- | This is returned as a pair, the first element being the move, the second being a Double that represents the rating
-- | alpha should start off as negInf
-- | beta should start off as posInf
-- |
alphaBeta :: String -> Double -> Double -> ((Char, Int), Double)
alphaBeta board alpha beta  | gameOver == Just 'X' = (nullMove, posInf)
                            | gameOver == Just 'O' = (nullMove, negInf)
                            | gameOver == Just ' ' = (nullMove, 0)
                            | gameOver == Nothing && playerToMove == 'X' = findMax (pruneSweep board possibleMoves alpha beta 'X') (nullMove, negInf)
                            | gameOver == Nothing && playerToMove == 'O' = findMin (pruneSweep board possibleMoves alpha beta 'O') (nullMove, posInf)
                            | otherwise = error "unexpected game state"
                            where gameOver = checkForGameOver board
                                  playerToMove = findPlayerToMove board
                                  possibleMoves = findPossibleMoves board

findMax :: [((Char, Int), Double)] -> ((Char, Int), Double) -> ((Char, Int), Double)
findMax [] curMax = curMax
findMax ((m, r):restOfOutcomes) (maxMove, maxRating) = if r > maxRating then findMax restOfOutcomes (m, r) else findMax restOfOutcomes (maxMove, maxRating)

findMin :: [((Char, Int), Double)] -> ((Char, Int), Double) -> ((Char, Int), Double)
findMin [] curMin = curMin
findMin ((m, r):restOfOutcomes) (minMove, minRating) = if r < minRating then findMin restOfOutcomes (m, r) else findMin restOfOutcomes (minMove, minRating)

-- |
-- | Help alphaBeta by stopping further searching when alpha-beta pruning must be triggered, resulting in faster execution
-- |
pruneSweep :: String -> [(Char, Int)] -> Double -> Double -> Char -> [((Char, Int), Double)]
pruneSweep _ [] _ _ _ = []
pruneSweep board (m:restOfMoves) alpha beta 'X' = let nextRating = snd (alphaBeta (applyMove board m) alpha beta) in
                                                 if nextRating > beta then
                                                     [(m, nextRating)]
                                                 else
                                                     (m, nextRating):(pruneSweep board restOfMoves nextRating beta 'X')
pruneSweep board (m:restOfMoves) alpha beta 'O' = let nextRating = snd (alphaBeta (applyMove board m) alpha beta) in
                                                 if nextRating < alpha then
                                                     [(m, nextRating)]
                                                 else
                                                     (m, nextRating):(pruneSweep board restOfMoves alpha nextRating 'O')

-- |
-- | Use alpha beta pruning to rate the value of a board state
-- | alpha should start off as negInf
-- | beta should start off as posInf
-- |
alphaBetaRater :: String -> Double -> Double -> Double
alphaBetaRater board alpha beta  | gameOver == Just 'X' = posInf
                                 | gameOver == Just 'O' = negInf
                                 | gameOver == Just ' ' = 0
                                 | gameOver == Nothing && playerToMove == 'X' = maximum $ pruneSweepRater nextBoardStates alpha beta 'X'
                                 | gameOver == Nothing && playerToMove == 'O' = minimum $ pruneSweepRater nextBoardStates alpha beta 'O'
                                 | otherwise = error "unexpected game state"
                                 where gameOver = checkForGameOver board
                                       playerToMove = findPlayerToMove board
                                       nextBoardStates = map (applyMove board) (findPossibleMoves board)

-- |
-- | Help alphaBetaRater by stopping further searching when alpha-beta pruning must be triggered, resulting in faster execution
-- |
pruneSweepRater :: [String] -> Double -> Double -> Char -> [Double]
pruneSweepRater [] _ _ _ = []
pruneSweepRater (b:boardStates) alpha beta 'X' = let nextRating = (alphaBetaRater b alpha beta) in
                                                 if nextRating > beta then
                                                     [nextRating]
                                                 else
                                                     nextRating:(pruneSweepRater boardStates nextRating beta 'X')
pruneSweepRater (b:boardStates) alpha beta 'O' = let nextRating = (alphaBetaRater b alpha beta) in
                                                 if nextRating < alpha then
                                                     [nextRating]
                                                 else
                                                     nextRating:(pruneSweepRater boardStates alpha nextRating 'O')

-- |
-- | The current board position is represented as a String, and a move is a pair with a Char and an Int.
-- |
-- | The Int is specifies the position to move in, with 0 meaning the top left, 1 meaning top middle, etc, 8 meaning bottom right.
-- | A move of X into the upper left corner is ('X', 0)
-- |
findBestMove :: String -> (Char, Int)
findBestMove board | length board /= 9 = error $ "Length of board must be 9. Actual length: " ++ show (length board)
                   | null (findPossibleMoves board) = error "Game over, no more moves are allowed"
                   | playerToMove == 'X' = fst . last . (rateEachMove board) $ findPossibleMoves board
                   | playerToMove == 'O' = fst . head . (rateEachMove board) $ findPossibleMoves board
                   where playerToMove = findPlayerToMove board

-- |
-- | The current board position is represented as a String, and a move is a pair with a Char and an Int.
-- |
-- | The Int is specifies the position to move in, with 0 meaning the top left, 1 meaning top middle, etc, 8 meaning bottom right.
-- | A move of X into the upper left corner is ('X', 0)
-- |
findBestMoveFast :: String -> (Char, Int)
findBestMoveFast board | length board /= 9 = error $ "Length of board must be 9. Actual length: " ++ show (length board)
                       | null (findPossibleMoves board) = error "Game over, no more moves are allowed"
                       | playerToMove == 'X' = fst . last . (rateEachMoveFast board) $ findPossibleMoves board
                       | playerToMove == 'O' = fst . head . (rateEachMoveFast board) $ findPossibleMoves board
                       where playerToMove = findPlayerToMove board

-- |
-- | Given a board and a list of possible moves, attach a rating to each move that expresses how good it is from X's perspective
-- | This list is returned sorted from lowest to highest rating
-- |
rateEachMove :: String -> [(Char, Int)] -> [((Char, Int), Double)]
rateEachMove board moves = sortOn (snd) $ zip moves $ map (rateBoard . (applyMove board)) moves

rateEachMoveFast :: String -> [(Char, Int)] -> [((Char, Int), Double)]
rateEachMoveFast board moves = sortOn (snd) $ zip moves $ map ((\b -> alphaBetaRater b negInf posInf) . (applyMove board)) moves

-- |
-- | Given a board and a move, make that move, which results in a new board
-- |
applyMove :: String -> (Char, Int) -> String
applyMove [] (c, i) = error "Index out of range"
applyMove (b:boardTail) (c, 0) = c:boardTail
applyMove (b:boardTail) (c, i) = b:(applyMove boardTail (c, i - 1))

-- |
-- | Given a board, find all possible moves that can be made next
-- |
findPossibleMoves :: String -> [(Char, Int)]
findPossibleMoves [] = error "Length of board is zero."
findPossibleMoves board = map (\x -> (findPlayerToMove board, x)) [i | i<-[0..(length board - 1)], board !! i == ' ']

-- |
-- | Given a board, determine whose turn it is to move next, either 'X' or 'O'
-- |
findPlayerToMove board = if tally <= 0 then 'X' else 'O'
                       where tally = sum $ map (\c -> case c of
                                                       'X' -> 1
                                                       'O' -> -1
                                                       _ -> 0) board

-- |
-- | Take a board position and return a rating. The more positive it is,
-- | the better the board seems to be for X, the more negative it is, the better things are for O.
-- |
rateBoard :: String -> Double

-- | It's easy to return a correct rating if the game is simply over and no more moves are allowed
rateBoard board | gameOverResult == Just 'X' = posInf
                | gameOverResult == Just 'O' = negInf
                | gameOverResult == Just ' ' = 0
                where gameOverResult = checkForGameOver board

-- | Return a rating when the game is not over yet by exploring every possible next move
-- TODO: this code is repeated in findBestMove: fst . last . (rateEachMove board) $ findPossibleMoves board
rateBoard board | playerToMove == 'X' = rateBoard $ applyMove board (fst . last . (rateEachMove board) $ findPossibleMoves board)
                | playerToMove == 'O' = rateBoard $ applyMove board (fst . head . (rateEachMove board) $ findPossibleMoves board)
                where playerToMove = findPlayerToMove board

-- |
-- | Return 'X' if the game is over and the X player has won, return 'O' if O won,
-- | ' ' if the game ended in a tie, and Nothing if the game isn't over yet.
-- |
checkForGameOver :: String -> Maybe Char
checkForGameOver board | winningChars == [] = if boardIsFull board then
                                                  Just ' '   -- the game has ended in a tie
                                              else
                                                  Nothing    -- the game is still in progress
                       | otherwise = Just (head winningChars)
                       where indicesToCheck = [[0,1,2], [3,4,5], [6,7,8], -- horizontal rows
                                               [0,3,6], [1,4,7], [2,5,8], -- vertical columns
                                               [0,4,8], [2,4,6]]          -- diagonals
                             winningChars = filter (/= ' ') [board !! head series | series <- indicesToCheck, valuesAreEqualAt board series]

-- |
-- | Return True if all the items in a list (xs) at the given indexes (indexes) are equal
-- |
valuesAreEqualAt :: Eq a => [a] -> [Int] -> Bool
valuesAreEqualAt xs indexes = and $ map (== head allValues) allValues
    where allValues = [xs !! i | i <- indexes]

-- |
-- | Return True if the board is full because no ' ' spaces exist
-- |
boardIsFull :: String -> Bool
boardIsFull board = and $ map (/= ' ') board

-- |
-- | Make the board printable by adding decoration and line breaks
-- |
boardToString :: String -> String
boardToString board = intercalate makeLineRow [makeXORow (takeThreeAt i) | i <- [0,3,6]]
    where makeXORow rowValues = (intercalate " | " $ map (:"") rowValues) ++ "\n"
          makeLineRow = "---------\n"
          takeThreeAt i = take 3 (drop i board)

-- |
-- | Turn a move, such as ('X', 0), into a String, such as "Place an X into the upper left"
-- |
moveToString :: (Char, Int) -> String
moveToString (c, n) = "Place an '" ++ [c] ++ "' in the " ++ positionToString n

-- |
-- | Convert a position into an English-language String that describes it. Table of positions on a tic tac toe board:
-- |
-- |     0 | 1 | 2
-- |     ---------
-- |     3 | 4 | 5
-- |     ---------
-- |     6 | 7 | 8
-- | 
positionToString :: Int -> String
positionToString n = case n of
                          0 -> "upper left corner"
                          1 -> "top middle"
                          2 -> "upper right corner"
                          3 -> "left middle"
                          4 -> "middle"
                          5 -> "right middle"
                          6 -> "lower left corner"
                          7 -> "bottom middle"
                          8 -> "lower right corner"
                          _ -> "unknown location"

-- |
-- | Given a one-line string that's supposed to represent the state of a board, strip away all characters that are not 'X', 'O', or ' '
-- |
stripNonXO :: String -> String
stripNonXO board = filter (`elem` "XO ") board

-- |
-- | Take a board state as a String and show it in the terminal, formatted nicely
-- |
showBoard board =
    putStrLn . boardToString $ board

-- |
-- | Given a board as input that is provided by the user and can have extraneous characters, such as "OXO---OXO---XOX",
-- | find the best next move to make and describe it in a human-readable way
-- |
showBestMove rawBoard =
    putStrLn output
        where board = stripNonXO rawBoard
              bestMove = findBestMove board
              output = (moveToString bestMove) ++ ", producing:\n" ++ (boardToString $ applyMove board bestMove) ++ "To show the next move, run: showBestMove \"" ++ (applyMove board bestMove) ++ "\""

-- |
-- | Given a board as input that is provided by the user and can have extraneous characters, such as "OXO---OXO---XOX",
-- | find the best next move to make and describe it in a human-readable way
-- |
showBestMoveFast rawBoard =
    putStrLn output
        where board = stripNonXO rawBoard
              bestMove = findBestMoveFast board
              output = (moveToString bestMove) ++ ", producing:\n" ++ (boardToString $ applyMove board bestMove) ++ "To show the next move, run: showBestMove \"" ++ (applyMove board bestMove) ++ "\""

-- |
-- | Have the computer play against itself until the game ends
-- |
findRemainder rawBoard | gameOverResult `elem` (map (Just) "XO ") = boardToString board
                       | otherwise = boardToString board ++ "\n==============================\n\n" ++ (findRemainder (applyMove board $ findBestMove board))
                       where board = stripNonXO rawBoard
                             gameOverResult = checkForGameOver board

showRemainder rawBoard = putStrLn (findRemainder rawBoard)

-- |
-- | Have the computer play against itself until the game ends
-- |
findRemainderFast rawBoard | gameOverResult `elem` (map (Just) "XO ") = boardToString board
                           | otherwise = boardToString board ++ "\n==============================\n\n" ++ (findRemainderFast (applyMove board $ findBestMoveFast board))
                       where board = stripNonXO rawBoard
                             gameOverResult = checkForGameOver board

showRemainderFast rawBoard = putStrLn (findRemainderFast rawBoard)

