-- |
-- | Define infinitely extreme board ratings
-- |
posInf = maxBound :: Int
negInf = minBound :: Int

-- |
-- | Use alpha beta pruning to find the value at the root of the given tree, represented as a series of nested lists
-- |
{-
alphaBeta :: (state, target) -> Int -> Int -> Bool -> Int
alphaBeta [t] _ _ _ = t
alphaBeta tree alpha beta True = maximum 
alphaBeta tree alpha beta isMaxer | leaves == [] = t
                                  | playerToMove == 'X' =
                                  | playerToMove == 'O' =
                                  where playerToMove = findPlayerToMove board
-}

{-
-- works only on a binary tree
alphaBeta :: [Int] -> Bool -> Int
alphaBeta [l] _ = l
alphaBeta [0,1] _ = error "0, 1 not pruned"
alphaBeta leaves isMaxer | isMaxer = if (length leaves) == 12 then
                                         maximum [(alphaBeta (take 4 leaves) False), (alphaBeta (take 4 (drop 4 leaves)) False), (alphaBeta (drop 8 leaves) False)]
                                     else
                                         max (alphaBeta (firstHalf leaves) False) (alphaBeta (secondHalf leaves) False)
                         | not isMaxer = min (alphaBeta (firstHalf leaves) True) (alphaBeta (secondHalf leaves) True)
                         where firstHalf l = take ((length l) `div` 2) l
                               secondHalf l = drop ((length l) `div` 2) l
-}

-- alpha should start off as negInf
-- beta should start off as posInf

alphaBeta :: [Int] -> Int -> Int -> Bool -> Int
alphaBeta [l] _ _ _ = l
alphaBeta leaves alpha beta True = if (length leaves) == 12 then
                                        thirdsOf leaves alpha beta
                                   else
                                        let leftValue = (alphaBeta (firstHalf leaves) alpha beta False) in
                                            if leftValue > beta then
                                                leftValue
                                            else
                                                max leftValue (alphaBeta (secondHalf leaves) leftValue beta False)
                                     
alphaBeta leaves alpha beta False = let leftValue = (alphaBeta (firstHalf leaves) alpha beta True) in
                                        if leftValue < alpha then
                                            leftValue
                                        else
                                            min leftValue (alphaBeta (secondHalf leaves) alpha leftValue True)

firstHalf l = take ((length l) `div` 2) l
secondHalf l = drop ((length l) `div` 2) l
thirdsOf l alpha beta = let leftValue = (alphaBeta (take 4 l) alpha beta False) in
                            if leftValue > beta then
                                leftValue
                            else
                                let midValue = (alphaBeta (take 4 (drop 4 l)) leftValue beta False) in
                                    if midValue > beta then
                                        midValue
                                    else
                                        maximum [leftValue, midValue, (alphaBeta (drop 8 l) (max leftValue midValue) beta False)]

data Direction = L | R
               deriving (Eq)

leafData :: [Int]
leafData = [4,6,7,9,1,2,0,1,8,1,9,2]

-- |
-- | Start at the root node of an imagined binary tree and land on the leaf data below
-- |
treeLander :: [Int] -> [Direction] -> Int
treeLander [] _ = -1        -- (-1) if there is no leaf data
treeLander [l] _ = l        -- If there is one piece of leaf data left, return it
treeLander (l:_) [] = l     -- If there are no more directions, return the head of the leaf data
treeLander leaves (d:moreDirs) | d == L = treeLander (firstHalf leaves) moreDirs
                               | d == R = treeLander (secondHalf leaves) moreDirs
                               where firstHalf l = take ((length l) `div` 2) l
                                     secondHalf l = drop ((length l) `div` 2) l

--attemptedTree = [ [[4,6], [7,9]], [[1,2] [0, 1]] ]

